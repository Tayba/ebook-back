const assert = require("assert")
const client = require("supertest")
const server = require("../srcs")

const semver = /^(0[1-9]?)|([1-9][0-9]*)(\.(0[1-9]?)|([1-9][0-9]*)){2}(\-[0-9a-z]([-.]?[0-9a-z]))?(\+[0-9a-z]([.-]?[0-9a-z]))?$/i

describe("ENDPOINTS", () => {
  describe("/version", () => {
    describe("GET", () => {
      it("Should return the version", done => {
        client(server).get("/version")
          .expect(200)
          .expect(({body}) => {
            assert(semver.test(body.version), "invalid version")
            assert(semver.test(body.db), "invalid db version")
            assert.equal(body.env, "test")
          })
          .end(done)
      })
    })
  })
})